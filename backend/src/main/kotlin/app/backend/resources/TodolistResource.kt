package app.backend.resources

import app.common.models.TodoItem
import app.common.models.TodoList
import mu.KLogging
import org.springframework.http.MediaType
import org.springframework.web.bind.annotation.CrossOrigin
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@CrossOrigin()
@RestController
@RequestMapping("/todolist")
class TodolistResource {

    companion object : KLogging()

    @CrossOrigin()
    @GetMapping("/example", produces = [MediaType.APPLICATION_JSON_VALUE])
    fun getLists(): TodoList{
        logger.info { "Getting example list" }
        return TodoList("Backend list", arrayOf(TodoItem("Expand TodoList class", 2), TodoItem("Drink beer", 1)))
    }
}

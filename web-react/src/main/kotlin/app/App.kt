package app

import app.todoList.service.TodoListService
import app.todoList.todoList
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import react.RBuilder
import react.RComponent
import react.RProps
import react.RState
import react.dom.div


class App : RComponent<RProps, RState>() {

    private val todoListService = TodoListService()

    init {
        GlobalScope.launch {
            val todoList = todoListService.getTodoList()
            console.log(todoList)
        }
    }

    override fun RBuilder.render() {
        div("content") {

                todoList(1)

        }
    }
}

fun RBuilder.app() = child(App::class) {}

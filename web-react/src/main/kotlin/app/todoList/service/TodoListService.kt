package app.todoList.service

import app.common.models.TodoList

class TodoListService: Service() {

    val baseUrl = "http://localhost:8070/todolist"

    suspend fun getTodoList(): TodoList{
        val response = httpGet("$baseUrl/example")
        return fromJSON(response)
    }



}

package app.android

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.TextView
import app.common.models.TodoList

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val todo = TodoList("Todolist", arrayOf())
        findViewById<TextView>(R.id.main_text).text = todo.name
    }
}

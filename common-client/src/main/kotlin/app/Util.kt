package app

import kotlinx.serialization.ImplicitReflectionSerializer
import kotlinx.serialization.Serializable
import kotlinx.serialization.json.JSON
import kotlinx.serialization.parse

class Util {

    @Serializable
    data class User(val name: String, val s: String)

    @ImplicitReflectionSerializer
    fun parse(json: String): User{
        return JSON.parse(json)
    }

}

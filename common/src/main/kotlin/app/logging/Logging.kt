package app.logging

class Logging {

    private val loggingImplementation = LoggingImplementation()

    fun log(message: String){
        loggingImplementation.log(message)
    }
}

expect class LoggingImplementation(){
    fun log(message: String)
}

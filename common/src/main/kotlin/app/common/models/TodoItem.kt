package app.common.models

data class TodoItem(
        val description: String,
        val priority: Int
)

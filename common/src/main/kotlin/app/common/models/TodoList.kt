package app.common.models

data class TodoList (
        val name: String,
        val items: Array<TodoItem>
)


